package model.fibonacci;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FibonacciCallable {
    private Fibonacci fibonacci = new Fibonacci(10);

    public void go() throws InterruptedException {

        Callable sum = () -> {
            int s = 0;
            for (int i = 0; i < fibonacci.getRow().length; i++) {
                s += fibonacci.getRow()[i];
                System.out.println(s);
                Thread.sleep(500);
            }
            return "" + s;
        };
        Callable goldenRatio = () -> {
            double s = 1;
            for (int i = 1; i < fibonacci.getRow().length; i++) {
                s = (double) fibonacci.getRow()[i] / (double) fibonacci.getRow()[i - 1];
                System.out.println(s);
                Thread.sleep(500);
            }
            return "" + s;
        };


        ExecutorService executorService = Executors.newWorkStealingPool();
        List<Callable<String>> callables = new ArrayList<>();
        callables.add(sum);
        callables.add(goldenRatio);
        executorService.invokeAll(callables)
                .stream().map(future -> {
            try {
                return future.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            return null;
        }).forEach(System.out::println);
    }
}
