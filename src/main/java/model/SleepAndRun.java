package model;

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.*;

public class SleepAndRun {
    private Scanner sc = new Scanner(System.in);
    Random random = new Random();

    private int getRandom() {
        return (random.nextInt(9) + 2);
    }


    public void start() {
        System.out.println("How many times you want sleep&run&?");
        int quantity = sc.nextInt();
        Callable callable = () -> {
            int time = getRandom();
            System.out.println("Hello!");
            return time;
        };
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        for (int i = 1; i <= quantity; i++) {
            ScheduledFuture<?> future = executor.schedule(callable, getRandom(), TimeUnit.SECONDS);
            long delay = future.getDelay(TimeUnit.SECONDS);
            System.out.println("Delay: " + delay + "s" + " for " + i + " run");
        }
        executor.shutdown();
    }
}

